using BusinessLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace BusinessLayer.Test
{
    [TestClass]
    public class PassportServiceTest
    {
        [TestMethod]
        public void ValidateAllResultTrue_ReturnSuccess_CaseOne()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "ZE0005093",
                Nationality = "CAN",
                DateOfBirth = new DateTime(1985, 01, 01),
                DateOfExpiry = new DateTime(2023, 01, 14),
                Gender = "F",
                MZR = "ZE00050932CAN8501019F2301147<<<<<<<<<<<<<<02"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x=>!x.Value).Any(), false);
        }

        [TestMethod]
        public void ValidateAllResultTrue_ReturnSuccess_CaseTwo()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "208796371",
                Nationality = "GBR",
                DateOfBirth = new DateTime(1965, 01, 23),
                DateOfExpiry = new DateTime(2016, 01, 01),
                Gender = "F",
                MZR = "2087963717GBR6501233F1601013<<<<<<<<<<<<<<04"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => !x.Value).Any(), false);
        }

        [TestMethod]
        public void Validate_ReturnSuccess_CaseThree()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreNotEqual(result.Where(x => !x.Value).Any(), true);
        }

        [TestMethod]
        public void ValidateGender_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "F",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => x.Name.Equals("Gender") && !x.Value).Any(), true);
        }

        [TestMethod]
        public void ValidateFinalDigit_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<02"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => x.Name.Equals("FinalCheckDigit") && !x.Value).Any(), true);
        }

        [TestMethod]
        public void ValidateNationality_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "GBR",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => x.Name.Equals("Nationality") && !x.Value).Any(), true);
        }

        [TestMethod]
        public void ValidatePassportId_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233453",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => !x.Value).Count(), 1);
        }

        [TestMethod]
        public void ValidatePassportIdMZR_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1980, 01, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334593AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => !x.Value).Count(), 3);
        }

        [TestMethod]
        public void ValidateDOB_ReturnFalse()
        {
            var PassportService = new PassportService();
            var Model = new Passport
            {
                PassportId = "A19233452",
                Nationality = "AFG",
                DateOfBirth = new DateTime(1979, 02, 25),
                DateOfExpiry = new DateTime(2022, 01, 24),
                Gender = "M",
                MZR = "A192334523AFG8001254M2201247<<<<<<<<<<<<<<00"
            };

            var result = PassportService.Validate(Model);
            Assert.AreEqual(result.Where(x => !x.Value).Count(), 1);
        }
    }
}
