﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using MRZValidator.WebService.Models;
using Microsoft.AspNetCore.Mvc;
using WebService.Utils.Converter;

namespace WebService.Controllers
{
    public class PassportController : Controller
    {
        private IPassportService _passportService;
        public PassportController(IPassportService passportService)
        {
            _passportService = passportService;
        }

        [HttpPost]
        public List<BusinessLayer.Models.Result> Validate([FromBody] PassportServiceModel passportViewModel)
        {
                var passportModel = new BusinessLayer.Models.Passport();
                ModelConverter.ConvertToModel(passportViewModel, passportModel);

                return _passportService.Validate(passportModel); //Call to Business Layer
        }
    }
}
