﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebService.Utils.Converter
{
    public static class ModelConverter
    {
        public static void ConvertToModel(MRZValidator.WebService.Models.PassportServiceModel passportViewModel, Passport passport)
        {
            passport.DateOfBirth = passportViewModel.DateOfBirth;
            passport.DateOfExpiry = passportViewModel.DateOfExpiry;
            passport.Gender = passportViewModel.Gender;
            passport.MZR = passportViewModel.MZR;
            passport.Nationality = passportViewModel.Nationality;
            passport.PassportId = passportViewModel.PassportId;
        }
    }
}
