﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRZValidator.WebService.Models
{
    public class PassportServiceModel
    {
        public string PassportId { get; set; }
        public string Nationality { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfExpiry { get; set; }
        public string MZR { get; set; }
    }
}
