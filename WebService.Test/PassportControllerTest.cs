using BusinessLayer;
using BusinessLayer.Models;
using MRZValidator.WebService.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using WebService.Controllers;

namespace WebService.Test
{
    [TestClass]
    public class PassportControllerTest
    {
        [TestMethod]
        public void ValidateInService()

        {
            Mock<IPassportService> _service = new Mock<IPassportService>();
            var PassportController = new PassportController(_service.Object);
            var serviceModel = new PassportServiceModel
            {
                PassportId = "ZE0005093",
                Nationality = "CAN",
                DateOfBirth = new DateTime(1985, 01, 01),
                DateOfExpiry = new DateTime(2023, 01, 14),
                Gender = "F",
                MZR = "ZE00050932CAN8501019F2301147<<<<<<<<<<<<<<02"
            };
            var model = new Passport
            {
                PassportId = "ZE0005093",
                Nationality = "CAN",
                DateOfBirth = new DateTime(1985, 01, 01),
                DateOfExpiry = new DateTime(2023, 01, 14),
                Gender = "F",
                MZR = "ZE00050932CAN8501019F2301147<<<<<<<<<<<<<<02"
            };
            _service.Setup(x => x.Validate(model)).Returns(new List<Result>());
            var result = PassportController.Validate(serviceModel);
            Assert.AreEqual(result, null);
        }
    }
}
