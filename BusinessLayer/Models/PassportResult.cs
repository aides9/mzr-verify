﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace BusinessLayer.Models
{
    public class PassportResult
    {
        [DisplayName("Passport No. Cross Check")]
        public string PassportId { get; set; }
        [DisplayName("Nationality Cross Check")]
        public string Nationality { get; set; }
        [DisplayName("Date Of Birth Cross Check")]
        public string DateOfBirth { get; set; }
        [DisplayName("Gender Cross Check")]
        public string Gender { get; set; }
        [DisplayName("Expiration Data Cross Check")]
        public string DateOfExpiry { get; set; }
        [DisplayName("Personal Number Check Digit")]
        public string PersonalNumber { get; set; }
        [DisplayName("Passport Number Check Digit")]
        public string FirstCheckDigit { get; set; }
        [DisplayName("Date of Birth Check Digit")]
        public string SecondCheckDigit { get; set; }
        [DisplayName("Passport Expiration Check Digit")]
        public string ThirdCheckDigit { get; set; }
        [DisplayName("Personal Number Check Digit")]
        public string FourthCheckDigit { get; set; }
        [DisplayName("Final Check Digit")]
        public string FinalCheckDigit { get; set; }
    }
}
