﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class MZRDecoded
    {
        public string PassportNumber { get; set; }
        public string FirstCheckDigit { get; set; }
        public string Nationality { get; set; }
        public string DateOfBirth { get; set; }
        public string SecondCheckDigit { get; set; }
        public string Gender { get; set; }
        public string ExpirationDate { get; set; }
        public string ThirdCheckDigit { get; set; }
        public string PersonalNumber { get; set; }
        public string FourthCheckDigit { get; set; }
        public string FinalCheckDigit { get; set; }
    }
}
