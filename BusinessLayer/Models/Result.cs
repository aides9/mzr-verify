﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class Result
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool Value { get; set; }
    }
}
