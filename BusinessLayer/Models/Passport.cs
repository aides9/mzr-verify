﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Models
{
    public class Passport
    {
        public string PassportId { get; set; }
        public string Nationality { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfExpiry { get; set; }
        public string MZR { get; set; }
        public string PersonalNumber { get; set; }
    }
}
