﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer
{
    public interface IPassportService
    {
        List<Result> Validate(Passport passport);
    }
}
