﻿using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace BusinessLayer
{
    public class PassportService : IPassportService
    {
        public PassportService() { }
        private List<Result> results { get; set; }

        public List<Result> Validate(Passport passport)
        {
            results = new List<Result>();
            var mZRDecoded = new MZRDecoded();
            parseMZR(passport.MZR, ref mZRDecoded);
            compareMZR(passport, mZRDecoded);
            return results;
        }

        #region Validate

        private void compareMZR(Passport passport, MZRDecoded mZRDecoded)
        {
            if (!(mZRDecoded.PassportNumber.Equals(passport.PassportId))) setResult(nameof(Passport.PassportId), false);
            if (!(mZRDecoded.Nationality.Equals(passport.Nationality))) setResult(nameof(Passport.Nationality), false);
            if (!(mZRDecoded.DateOfBirth.Equals(passport.DateOfBirth.ToString("yyMMdd")))) setResult(nameof(Passport.DateOfBirth), false);
            if (!(mZRDecoded.Gender.Equals(passport.Gender))) setResult(nameof(Passport.Gender), false);
            if (!(mZRDecoded.ExpirationDate.Equals(passport.DateOfExpiry.ToString("yyMMdd")))) setResult(nameof(Passport.DateOfExpiry), false);
        }

        private void parseMZR(string MZR, ref MZRDecoded mZRDecoded)
        {
            mZRDecoded.PassportNumber = MZR.Substring(0, 9);
            mZRDecoded.FirstCheckDigit = MZR.Substring(9, 1);
            mZRDecoded.Nationality = MZR.Substring(10, 3);
            mZRDecoded.DateOfBirth = MZR.Substring(13, 6);
            mZRDecoded.SecondCheckDigit = MZR.Substring(19, 1);
            mZRDecoded.Gender = MZR.Substring(20, 1);
            mZRDecoded.ExpirationDate = MZR.Substring(21, 6);
            mZRDecoded.ThirdCheckDigit = MZR.Substring(27, 1);
            mZRDecoded.PersonalNumber = MZR.Substring(28, 14);
            mZRDecoded.FourthCheckDigit = MZR.Substring(42, 1);
            mZRDecoded.FinalCheckDigit = MZR.Substring(43, 1);

            IsPassportNumber(mZRDecoded.PassportNumber);
            IsCheckDigit(nameof(MZRDecoded.FirstCheckDigit), mZRDecoded.FirstCheckDigit, mZRDecoded.PassportNumber);
            IsNationality(mZRDecoded.Nationality);
            IsDob(mZRDecoded.DateOfBirth);
            IsCheckDigit(nameof(MZRDecoded.SecondCheckDigit), mZRDecoded.SecondCheckDigit, mZRDecoded.DateOfBirth);
            IsGender(mZRDecoded.Gender);
            IsExpirationDate(mZRDecoded.ExpirationDate);
            IsCheckDigit(nameof(MZRDecoded.ThirdCheckDigit), mZRDecoded.ThirdCheckDigit, mZRDecoded.ExpirationDate);
            IsPersonalNumber(mZRDecoded.PersonalNumber);
            IsCheckDigit(nameof(MZRDecoded.FourthCheckDigit), mZRDecoded.FourthCheckDigit, mZRDecoded.PersonalNumber);
            IsCheckDigit(nameof(MZRDecoded.FinalCheckDigit), mZRDecoded.FinalCheckDigit, MZR.Substring(0, 10) + MZR.Substring(13, 7) + MZR.Substring(21, 12));
        }

        #endregion

        #region ParseCheck

        private void IsPassportNumber(string number)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            setResult(nameof(Passport.PassportId), r.IsMatch(number));
        }


        private void IsCheckDigit(string name, string digit, string input)
        {
            int number;
            setResult(name, (int.TryParse(digit, out number) && GenerateCheckDigit(input) == number));
        }

        private void IsNationality(string nationality)
        {
            Regex r = new Regex("^[a-zA-Z]+$");
            setResult(nameof(Passport.Nationality), r.IsMatch(nationality));
        }
        private void IsDob(string date)
        {
            DateTime dob;
            setResult(nameof(Passport.DateOfBirth), DateTime.TryParseExact(date, "yyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dob) && dob < DateTime.Now);
        }

        private void IsGender(string gender)
        {
            char f = 'f';
            char m = 'm';
            var genderchar = gender.ToCharArray()[0];
            setResult(nameof(Passport.Gender), char.ToLowerInvariant(genderchar) == f || char.ToLowerInvariant(genderchar) == m);
        }

        private void IsExpirationDate(string date)
        {
            DateTime expirationDate;
            setResult(nameof(Passport.DateOfExpiry), DateTime.TryParseExact(date, "yyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out expirationDate));
        }

        private void IsPersonalNumber(string number)
        {
            Regex r = new Regex("^[a-zA-Z0-9<]*$");
            setResult(nameof(Passport.PersonalNumber), r.IsMatch(number));
        }

        #endregion

        #region CheckDigitVerification

        private int GenerateCheckDigit(string input)
        {
            var inputs = input.ToCharArray();
            var digits = new List<int>();

            foreach (var inputCharacter in inputs)
            {
                var matchedValue = GetDigitValueMap().Where(x => x.Key.Equals(Char.ToUpper(inputCharacter)));
                if (matchedValue.Any())
                {
                    digits.Add(matchedValue.First().Value);
                }
            }

            var checkDigit = digits.Zip(GetMultiplicationList(digits.Count()), (first, second) => first * second)
                                        .Aggregate(0, (acc, x) => acc + x) % 10;
            return checkDigit;
        }


        private static List<int> GetMultiplicationList(int length)
        {
            int[] checkDigitMultiplication = new int[3] { 7, 3, 1 };
            int cycle = 0;
            var multiplicationList = new List<int>();
            for (int u = 0; u < length; u++)
            {
                multiplicationList.Add(checkDigitMultiplication[cycle]);
                cycle++;
                if(cycle >= 3)
                {
                    cycle = 0;
                }
            }

            return multiplicationList;
        }

        private static Dictionary<char, int> GetDigitValueMap()
        {
            return new Dictionary<char, int>() {
            {'0',0},
            {'1',1},
            {'2',2},
            {'3',3},
            {'4',4},
            {'5',5},
            {'6',6},
            {'7',7},
            {'8',8},
            {'9',9},
            {'<',0},
            {'A',10},
            {'B',11},
            {'C',12},
            {'D',13},
            {'E',14},
            {'F',15},
            {'G',16},
            {'H',17},
            {'I',18},
            {'J',19},
            {'K',20},
            {'L',21},
            {'M',22},
            {'N',23},
            {'O',24},
            {'P',25},
            {'Q',26},
            {'R',27},
            {'S',28},
            {'T',29},
            {'U',30},
            {'V',31},
            {'W',32},
            {'X',33},
            {'Y',34},
            {'Z',35}
            };
        }

        #endregion

        #region ResultHandling

        private void setResult(string resultName, bool resultValue = false)
        {
            var existResult = results.FindIndex(x => x.Name.Equals(resultName));
            if (existResult != -1)
            {
                results[existResult].Value = resultValue;
            }
            else
            {
                results.Add(new Result { Name = resultName, Value = resultValue });
            }
        }

        #endregion   
    }
}
