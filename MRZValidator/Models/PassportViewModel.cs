﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MRZValidator.Models
{
    public class PassportViewModel
    {
        [Required(ErrorMessage = "Required")]
        [StringLength(9, MinimumLength = 9, ErrorMessage = "Passport Number must be 9 character")]
        public string PassportId { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Nationality  must 3 character")]
        public string Nationality { get; set; }
        public DateTime DateOfBirth { get { return new DateTime(DobYear??1900, DobMonth??1, DobDay??1); } set {; } }
        [Required(ErrorMessage ="Required")]
        [Range(1, 31, ErrorMessage = "Invalid Number")]
        public int? DobDay { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1, 12, ErrorMessage = "Invalid Number")]
        public int? DobMonth { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1900, 3000, ErrorMessage = "Invalid Number")]
        public int? DobYear { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(1)]
        public string Gender { get; set; }
        public DateTime DateOfExpiry { get { return new DateTime(DoeYear ?? 1900, DoeMonth ?? 1, DoeDay ?? 1); } set {; } }
        [Required(ErrorMessage = "Required")]
        [Range(1, 31, ErrorMessage = "Invalid Number")]
        public int? DoeDay { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1, 12, ErrorMessage = "Invalid Number")]
        public int? DoeMonth { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1900, 3000, ErrorMessage = "Invalid Number")]
        public int? DoeYear { get; set; }
        [Required(ErrorMessage = "Required")]
        [StringLength(44, MinimumLength = 44,ErrorMessage = "MZR must be 44 character")]
        public string MZR { get; set; }
    }
}
