﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MRZValidator.Models
{
    public class ResultViewModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool Value { get; set; }

    }
}
