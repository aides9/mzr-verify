﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MRZValidator.Models;
using System.Net.Http;
using System.Web;
using Microsoft.AspNetCore.Http.Extensions;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;
using MRZValidator.Enums;
using System.Net;
using System.Reflection;
using System.ComponentModel;
using Microsoft.Extensions.Configuration;
using System.Globalization;

namespace MRZValidator.Controllers
{
    public class HomeController : Controller
    {
        private IConfiguration _configuration;
        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            SetViewBag();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Validate([Bind(include: "PassportId, Nationality, DobDay, DobMonth, DobYear, Gender, DoeDay, DoeMonth, DoeYear, MZR")] PassportViewModel passport)
        {
            if (ModelState.IsValid)
            {
                const string apiUrl = "/api/Passport/Validate";

                var webServiceUrl = string.Format("{0}{1}", _configuration["WebService"], apiUrl); //Get BaseUrl path from appsetting.json
               

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    //Set url, content and post json to web service 
                    client.BaseAddress = new Uri(webServiceUrl);
                    var content = new StringContent(JsonConvert.SerializeObject(passport).ToString(), Encoding.UTF8, "application/json"); //serialize
                    try
                    {
                        response = await client.PostAsync(apiUrl, content); //Please run WebService as new instance before call
                    }
                    catch (Exception) //exception handling when WebService never open
                    {
                        ViewData["ErrorMessage"] = "Service is Offline";
                        SetViewBag();
                        return View("index", passport);
                    }
                    if (response.IsSuccessStatusCode)
                    {
                        ClearValidationMessage(); //clear previous error validation
                        var data = await response.Content.ReadAsStringAsync(); //Read json message response by web service
                        var results = JsonConvert.DeserializeObject<List<ResultViewModel>>(data); // deserialize
                        SetDisplayName(results);
                        return View("result", results); // redirect to result page
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        ViewData["ErrorMessage"] = "Service is Offline";
                    }
                    else if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        var results = JsonConvert.DeserializeObject<ErrorViewModel>(data);
                        ViewData["ErrorMessage"] = results.Message;
                    }
                }
            }
            SetViewBag();
            return View("index",passport);
        }
        
        private void ClearValidationMessage()
        {
            ViewData.Clear();
            ModelState.Clear();
        }

        private void SetViewBag()
        {
            //Generate drop down list item
            var yearList = Enumerable.Range(1900, 2050);
            var monthList = Enumerable.Range(1, 12).Select(x => new SelectListItem()
            {
                Text = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames[x - 1],
                Value = x.ToString()
            });
            var dayList = Enumerable.Range(1, 31);
            ViewBag.DobYear = new SelectList(yearList);
            ViewBag.DobMonth = new SelectList(monthList, "Value", "Text", 1);
            ViewBag.DobDay = new SelectList(dayList);
            ViewBag.DoeYear = new SelectList(yearList);
            ViewBag.DoeMonth = new SelectList(monthList, "Value", "Text", 1);
            ViewBag.DoeDay = new SelectList(dayList);
            ViewBag.Gender = new SelectList(Enum.GetValues(typeof(Gender)));
        }
        
        private void SetDisplayName (List<ResultViewModel> results)
        {
            //Set Result Name in result page
            foreach (var result in results)
            {
                result.DisplayName = getDisplayName(result.Name);
            }
        }

        private static string getDisplayName(string resultName)
        {
            //Get Display value from Display Name Annotation
            MemberInfo property = typeof(PassportResultViewModel).GetProperty(resultName);
            var attribute = property.GetCustomAttributes(typeof(DisplayNameAttribute), true).Cast<DisplayNameAttribute>().Single();
            return attribute.DisplayName;
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
